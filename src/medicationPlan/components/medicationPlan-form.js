import React from 'react';
import validate from "./validators/medicationPlan-validators";
import Button from "react-bootstrap/Button";
import * as API_USERS from "../api/medicationPlan-api";
//import api de la pacienti si de la medications
import APIResponseErrorMessage from "../../commons/errorhandling/api-response-error-message";
import {Col, Row} from "reactstrap";
import { FormGroup, Input, Label} from 'reactstrap';



class MedicationPlanForm extends React.Component {

    constructor(props) {
        super(props);
        this.toggleForm = this.toggleForm.bind(this);
        this.reloadHandler = this.props.reloadHandler;

        this.state = {

            errorStatus: 0,
            error: null,

            formIsValid: false,

            formControls: {
                name: {
                    value: '',
                    placeholder: 'What is your name?...',
                    valid: false,
                    touched: false,
                    validationRules: {
                        minLength: 3,
                        isRequired: true
                    }
                },
                intakeIntervals: {
                     value: '',
                     placeholder: 'IntakeIntervals:',
                     valid: false,
                     touched: false,
                     validationRules: {
                         minLength: 3,
                         isRequired: true
                     }
                },
                period: {
                     value: '',
                     placeholder: 'Period:',
                     valid: false,
                     touched: false,
                      validationRules: {
                            isRequired: true
                      }
                },
                listOfMedications: {
                    value: '',
                    placeholder: 'List with medication:',
                    valid: false,
                    touched: false,
                    validationRules: {
                         minLength: 3,
                         isRequired: true
                    }
                },
                patient: {
                    value: '',
                    placeholder: 'Name of the patient',
                    valid: false,
                    touched: false,
                },
            }
        };

        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    toggleForm() {
        this.setState({collapseForm: !this.state.collapseForm});
    }


    handleChange = event => {

        const name = event.target.name;
        const value = event.target.value;

        const updatedControls = this.state.formControls;

        const updatedFormElement = updatedControls[name];

        updatedFormElement.value = value;
        updatedFormElement.touched = true;
        updatedFormElement.valid = validate(value, updatedFormElement.validationRules);
        updatedControls[name] = updatedFormElement;

        let formIsValid = true;
        for (let updatedFormElementName in updatedControls) {
            formIsValid = updatedControls[updatedFormElementName].valid && formIsValid;
        }

        this.setState({
            formControls: updatedControls,
            formIsValid: formIsValid
        });

    };

    registerMedicationPlan(medicationPlan) {
        return API_USERS.postMedicationPlan(medicationPlan, (result, status, error) => {
            if (result !== null && (status === 200 || status === 201)) {
                console.log("Successfully inserted medication plan with id: " + result);
                this.reloadHandler();
            } else {
                this.setState(({
                    errorStatus: status,
                    error: error
                }));
            }
        });
    }

    handleSubmit() {

            API_USERS.getPatientByName(this.state.formControls.patient.value, (result, status, error) => {
            if (result !== null && (status === 200 || status === 201)) {
                let patientDTO=result;
                console.log(patientDTO);
                API_USERS.getMedicationByName(this.state.formControls.listOfMedications.value, (result, status, error) => {
                                    if (result !== null && (status === 200 || status === 201)) {
                                        let med=result;
                                        console.log(med);
                                        let medicationPlan = {
                                                    name: this.state.formControls.name.value,
                                                    intakeIntervals: this.state.formControls.intakeIntervals.value,
                                                    period: this.state.formControls.period.value,
                                                    listOfMedications: med,
                                                    patientDTO: patientDTO
                                                };

                                                console.log(medicationPlan);
                                                this.registerMedicationPlan(medicationPlan);
                                    } else {
                                        this.setState(({
                                            errorStatus: status,
                                            error: error
                                        }));
                                    }
                                });
            } else {
                this.setState(({
                    errorStatus: status,
                    error: error
                }));
            }
        });


        //patient  /// trebuie cumva sa dau un findByName si sa imi pun intreg pacientul in constructor
        //listOfMedications // trebuie cumva sa iau fiecare nume si sa caut medication ul din baza de date


    }

    render() {
        return (
            <div>

                <FormGroup id='name'>
                    <Label for='nameField'> Name: </Label>
                    <Input name='name' id='nameField' placeholder={this.state.formControls.name.placeholder}
                           onChange={this.handleChange}
                           defaultValue={this.state.formControls.name.value}
                           touched={this.state.formControls.name.touched? 1 : 0}
                           valid={this.state.formControls.name.valid}
                           required
                    />
                    {this.state.formControls.name.touched && !this.state.formControls.name.valid &&
                    <div className={"error-message row"}> * Name must have at least 3 characters </div>}
                </FormGroup>

                <FormGroup id='intakeIntervals'>
                    <Label for='intakeIntervalsField'> intakeIntervals: </Label>
                    <Input name='intakeIntervals' id='intakeIntervalsField' placeholder={this.state.formControls.intakeIntervals.placeholder}
                           onChange={this.handleChange}
                           defaultValue={this.state.formControls.intakeIntervals.value}
                           touched={this.state.formControls.intakeIntervals.touched? 1 : 0}
                           valid={this.state.formControls.intakeIntervals.valid}
                           required
                    />
                    {this.state.formControls.intakeIntervals.touched && !this.state.formControls.intakeIntervals.valid &&
                     <div className={"error-message row"}> * intakeIntervals must have at least 3 characters </div>}
                </FormGroup>

                <FormGroup id='period'>
                     <Label for='periodField'> Period: </Label>
                     <Input name='period' id='periodField' placeholder={this.state.formControls.period.placeholder}
                            onChange={this.handleChange}
                            defaultValue={this.state.formControls.period.value}
                            touched={this.state.formControls.period.touched? 1 : 0}
                            valid={this.state.formControls.period.valid}
                            required
                     />
                     {this.state.formControls.period.touched && !this.state.formControls.period.valid &&
                     <div className={"error-message row"}> * Period must have at least 3 characters </div>}
                </FormGroup>

                <FormGroup id='listOfMedications'>
                     <Label for='listOfMedicationsField'> listOfMedications: </Label>
                     <Input name='listOfMedications' id='listOfMedicationsField' placeholder={this.state.formControls.listOfMedications.placeholder}
                            onChange={this.handleChange}
                            defaultValue={this.state.formControls.listOfMedications.value}
                            touched={this.state.formControls.listOfMedications.touched? 1 : 0}
                            valid={this.state.formControls.listOfMedications.valid}
                            required
                     />
                     {this.state.formControls.listOfMedications.touched && !this.state.formControls.listOfMedications.valid &&
                     <div className={"error-message row"}> * ListOfMedications must have at least 3 characters </div>}
                </FormGroup>


                <FormGroup id='patient'>
                    <Label for='patientField'> Pacientul: </Label>
                    <Input name='patient' id='patientField' placeholder={this.state.formControls.patient.placeholder}
                           onChange={this.handleChange}
                           defaultValue={this.state.formControls.patient.value}
                           touched={this.state.formControls.patient.touched? 1 : 0}
                           valid={this.state.formControls.patient.valid}
                           required
                    />
                </FormGroup>


                    <Row>
                        <Col sm={{size: '4', offset: 8}}>
                            <Button type={"submit"} disabled={!this.state.formIsValid} onClick={this.handleSubmit}>  Submit </Button>
                        </Col>
                    </Row>

                {
                    this.state.errorStatus > 0 &&
                    <APIResponseErrorMessage errorStatus={this.state.errorStatus} error={this.state.error}/>
                }
            </div>
        ) ;
    }
}

export default MedicationPlanForm;
