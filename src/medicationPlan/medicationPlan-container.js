import React from 'react';
import APIResponseErrorMessage from "../commons/errorhandling/api-response-error-message";
import {
    Button,
    Card,
    CardHeader,
    Col,
    Modal,
    ModalBody,
    ModalHeader,
    Row
} from 'reactstrap';
import MedicationPlanForm from "./components/medicationPlan-form";


import * as API_USERS from "./api/medicationPlan-api"
import MedicationPlanTable from "./components/medicationPlan-table";



class MedicationPlanContainer extends React.Component {

    constructor(props) {
        super(props);
        this.toggleForm = this.toggleForm.bind(this);
        this.reload = this.reload.bind(this);
          this.state = {
            selected: false,
            collapseForm: false,
            tableData: [],
            isLoaded: false,
            errorStatus: 0,
            error: null
        };
    }

    componentDidMount() {
        this.fetchMedicationPlans();
    }

    fetchMedicationPlans() {
        return API_USERS.getMedicationPlans((result, status, err) => {

            if (result !== null && status === 200) {
                let result2=result.map(this.mappingMed);
                this.setState({
                    tableData: result2,
                    isLoaded: true
                });
            } else {
                this.setState(({
                    errorStatus: status,
                    error: err
                }));
            }
        });
    }

         mappingMed = (object) => {
           let lista=[];
           object.listOfMedications.map((medicationObject,index) => {
            lista.push(medicationObject.name);
           });
           lista.join(',');
           let name="";
           if(object.patientDTO===null){
           name="-";
           }
           else
           name=object.patientDTO.name;
           var obj=   {
                    name:object.name,
                    intakeIntervals:object.intakeIntervals,
                    period: object.period,
                    listOfMedications:lista,
                    patient:name,
                    };

            return obj;

        }

    toggleForm() {
        this.setState({selected: !this.state.selected});
    }


    reload() {
        this.setState({
            isLoaded: false
        });
        this.toggleForm();

        this.fetchMedicationPlans();
    }

    render() {
        return (
            <div>
                <CardHeader>
                    <strong> Medication Plan Management </strong>
                </CardHeader>
                <Card>
                    <br/>
                    <Row>
                        <Col sm={{size: '8', offset: 1}}>
                            <Button color="primary" onClick={this.toggleForm}>Add Medication Plan</Button>

                        </Col>
                    </Row>
                    <br/>
                    <Row>
                        <Col sm={{size: '8', offset: 1}}>
                            {this.state.isLoaded && <MedicationPlanTable tableData = {this.state.tableData}/>}
                            {this.state.errorStatus > 0 && <APIResponseErrorMessage
                                                            errorStatus={this.state.errorStatus}
                                                            error={this.state.error}
                                                        />   }
                        </Col>
                    </Row>
                </Card>

                <Modal isOpen={this.state.selected} toggle={this.toggleForm}
                       className={this.props.className} size="lg">
                    <ModalHeader toggle={this.toggleForm}> Add Medication Plan: </ModalHeader>
                    <ModalBody>
                        <MedicationPlanForm reloadHandler={this.reload}/>
                    </ModalBody>
                </Modal>


            </div>
        )

    }
}


export default MedicationPlanContainer;
