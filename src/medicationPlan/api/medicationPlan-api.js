import {HOST} from '../../commons/hosts';
import RestApiClient from "../../commons/api/rest-client";


const endpoint = {
    getAllMedicationPlans:'/medicationPlan/allMedicationPlans',
    insertMedicationPlan: '/medicationPlan/insert',
    getPatient:'/patient/getPatient/',
    getMedication:'/medication/getMedication/'
};

function getMedicationPlans(callback) {
    let request = new Request(HOST.backend_api + endpoint.getAllMedicationPlans, {
        method: 'GET',
    });
    console.log(request.url);
    RestApiClient.performRequest(request, callback);
}


function postMedicationPlan(user, callback){
    let request = new Request(HOST.backend_api + endpoint.insertMedicationPlan , {
        method: 'POST',
        headers : {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
        },
        body: JSON.stringify(user)
    });

    console.log("URL: " + request.url);

    RestApiClient.performRequest(request, callback);
}

function getPatientByName(name, callback){
    let request = new Request(HOST.backend_api + endpoint.getPatient+name, {
        method: 'GET',
    });
    console.log(request.url);
    RestApiClient.performRequest(request, callback);
}

function getMedicationByName(name, callback){
    let request = new Request(HOST.backend_api + endpoint.getMedication+name, {
        method: 'GET',
    });
    console.log(request.url);
    RestApiClient.performRequest(request, callback);
}


export {
    getMedicationPlans,
    postMedicationPlan,
    getPatientByName,
    getMedicationByName

};
