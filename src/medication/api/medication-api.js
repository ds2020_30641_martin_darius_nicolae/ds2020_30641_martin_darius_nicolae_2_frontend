import {HOST} from '../../commons/hosts';
import RestApiClient from "../../commons/api/rest-client";


const endpoint = {
    medication: '/medication',
    getAllMedications:'/medication/allMedications',
    insertMedication: '/medication/insert',
    deleteMedication: '/medication/delete/',
    updateMedication: '/medication/update/'
};

function getMedications(callback) {
    let request = new Request(HOST.backend_api + endpoint.getAllMedications, {
        method: 'GET',
    });
    console.log(request.url);
    RestApiClient.performRequest(request, callback);
}


function postMedication(user, callback){
    let request = new Request(HOST.backend_api + endpoint.insertMedication , {
        method: 'POST',
        headers : {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
        },
        body: JSON.stringify(user)
    });

    console.log("URL: " + request.url);

    RestApiClient.performRequest(request, callback);
}

function deleteMedication(name,callback){
    let request = new Request(HOST.backend_api + endpoint.deleteMedication+name, {
        method: 'DELETE',
    });

    console.log("URL: " + request.url);

    RestApiClient.performRequest(request, callback);
}

function updateMedication(name, user, callback){
    let request = new Request(HOST.backend_api + endpoint.updateMedication+name , {
        method: 'PUT',
        headers : {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
        },
        body: JSON.stringify(user)
    });

    console.log("URL: " + request.url);

    RestApiClient.performRequest(request, callback);
}

export {
    getMedications,
    postMedication,
    deleteMedication,
    updateMedication
};
