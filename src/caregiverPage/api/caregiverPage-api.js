import {HOST} from '../../commons/hosts';
import RestApiClient from "../../commons/api/rest-client";


const endpoint = {
    caregiverPage: '/caregiver/AllPatients/',
    hire:'/caregiver/PacientToCaregiver',
};

function getPatients(name,callback) {
    let request = new Request(HOST.backend_api + endpoint.caregiverPage+name, {
        method: 'GET',
    });
    console.log(request.url);
    RestApiClient.performRequest(request, callback);
}

function hireCaregiver(namePatient,nameCaregiver, callback){
    let request = new Request(HOST.backend_api + endpoint.hire+"?numePacient="+namePatient+"&numeCaregiver="+nameCaregiver , {
        method: 'POST',
        headers : {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
        },
    });

    console.log("URL: " + request.url);

    RestApiClient.performRequest(request, callback);
}

export {
    getPatients,
    hireCaregiver,
};