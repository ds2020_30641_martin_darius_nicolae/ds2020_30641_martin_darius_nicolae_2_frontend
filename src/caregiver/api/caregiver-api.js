import {HOST} from '../../commons/hosts';
import RestApiClient from "../../commons/api/rest-client";


const endpoint = {
    caregiver: '/caregiver',
    getAllCaregivers:'/caregiver/allCaregivers',
    insertCaregiver: '/caregiver/insert',
    deleteCaregiver: '/caregiver/delete/',
    updateCaregiver: '/caregiver/update/'

};

function getCaregivers(callback) {
    let request = new Request(HOST.backend_api + endpoint.getAllCaregivers, {
        method: 'GET',
    });
    console.log(request.url);
    RestApiClient.performRequest(request, callback);
}


function postCaregiver(user, callback){
    let request = new Request(HOST.backend_api + endpoint.insertCaregiver , {
        method: 'POST',
        headers : {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
        },
        body: JSON.stringify(user)
    });

    console.log("URL: " + request.url);

    RestApiClient.performRequest(request, callback);
}

function deleteCaregiver(name,callback){
    let request = new Request(HOST.backend_api + endpoint.deleteCaregiver+name, {
        method: 'DELETE',
    });

    console.log("URL: " + request.url);

    RestApiClient.performRequest(request, callback);
}

function updateCaregiver(name, user, callback){
    let request = new Request(HOST.backend_api + endpoint.updateCaregiver + name , {
        method: 'PUT',
        headers : {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
        },
        body: JSON.stringify(user)
    });


    console.log("URL: " + request.url);

    RestApiClient.performRequest(request, callback);
}

export {
    getCaregivers,
    postCaregiver,
    deleteCaregiver,
    updateCaregiver
};
