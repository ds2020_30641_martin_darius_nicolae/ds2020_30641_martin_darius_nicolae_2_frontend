import React from 'react';
import validate from "./validators/login-validators";
import Button from "react-bootstrap/Button";
import * as API_USERS from "../api/login-api";
import APIResponseErrorMessage from "../../commons/errorhandling/api-response-error-message";
import {Col, Row} from "reactstrap";
import { FormGroup, Input, Label} from 'reactstrap';
import {createBrowserHistory} from "history";

const history = createBrowserHistory({forceRefresh: true});



class LoginForm extends React.Component {

    constructor(props) {
        super(props);
        this.toggleForm = this.toggleForm.bind(this);
        this.reloadHandler = this.props.reloadHandler;

        this.state = {

            errorStatus: 0,
            error: null,

            formIsValid: false,

            formControls: {
                email: {
                    value: '',
                    placeholder: 'Email...',
                    valid: false,
                    touched: false,
                    validationRules: {
                        emailValidator: true
                    }
                },
                password: {
                    value: '',
                    placeholder: 'Password:',
                    valid: false,
                    touched: false,
                    validationRules: {
                         minLength: 3,
                         isRequired: true
                    }
                },

            }
        };

        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    toggleForm() {
        this.setState({collapseForm: !this.state.collapseForm});
    }


    handleChange = event => {

        const name = event.target.name;
        const value = event.target.value;

        const updatedControls = this.state.formControls;

        const updatedFormElement = updatedControls[name];

        updatedFormElement.value = value;
        updatedFormElement.touched = true;
        updatedFormElement.valid = validate(value, updatedFormElement.validationRules);
        updatedControls[name] = updatedFormElement;

        let formIsValid = true;
        for (let updatedFormElementName in updatedControls) {
            formIsValid = updatedControls[updatedFormElementName].valid && formIsValid;
        }

        this.setState({
            formControls: updatedControls,
            formIsValid: formIsValid
        });

    };

    login(email, password) {
        return API_USERS.login(email,password, (result, status, error) => {
            if (result !== null && status === 202) {
                console.log("Successfully deleted patient with id: " + result);
                let id=result;
                 return API_USERS.getPatientById(id, (result, status, error) => {
                            if (result !== null && (status === 200 || status === 201)) {
                                history.push(`/patientPage/${result["name"]}`);
                                console.log("Am gasit pacientul");
                            } else {
                                return API_USERS.getCaregiverById(id, (result, status, error) => {
                                         if (result !== null && (status === 200 || status === 201)) {
                                               history.push(`/caregiverPage/${result["name"]}`);
                                               console.log("Am gasit caregiverul");
                                         } else {
                                                return API_USERS.getDoctorById(id, (result, status, error) => {
                                                          if (result !== null && (status === 200 || status === 201)) {
                                                                 history.push(`/doctorPage`);
                                                          } else {
                                                                  this.setState(({
                                                                       errorStatus: status,
                                                                       error: error
                                                                  }));
                                                          }
                                                });
                                         }
                                });
                            }
                 });
            } else {
                this.setState(({
                    errorStatus: status,
                    error: error
                }));
            }
        });
    }

    handleSubmit() {
        let email = this.state.formControls.email.value;
        let password=this.state.formControls.password.value;
        console.log(email);
        console.log(password);
        this.login(email,password);
    }

    render() {
        return (
            <div>

                <FormGroup id='email'>
                    <Label for='emailField'> Email: </Label>
                    <Input name='email' id='emailField' placeholder={this.state.formControls.email.placeholder}
                           onChange={this.handleChange}
                           defaultValue={this.state.formControls.email.value}
                           touched={this.state.formControls.email.touched? 1 : 0}
                           valid={this.state.formControls.email.valid}
                           required
                    />
                    {this.state.formControls.email.touched && !this.state.formControls.email.valid &&
                    <div className={"error-message"}> * Email must have a valid format</div>}
                </FormGroup>

                <FormGroup id='password'>
                     <Label for='passwordField'> Password: </Label>
                     <Input name='password' id='passwordField' placeholder={this.state.formControls.password.placeholder}
                            onChange={this.handleChange}
                            defaultValue={this.state.formControls.password.value}
                            touched={this.state.formControls.password.touched? 1 : 0}
                            valid={this.state.formControls.password.valid}
                            required
                     />
                     {this.state.formControls.password.touched && !this.state.formControls.password.valid &&
                     <div className={"error-message row"}> * Password must have at least 3 characters </div>}
                </FormGroup>

                    <Row>
                        <Col sm={{size: '4', offset: 8}}>
                            <Button type={"submit"} disabled={!this.state.formIsValid} onClick={this.handleSubmit}>  Submit </Button>
                        </Col>
                    </Row>

                {
                    this.state.errorStatus > 0 &&
                    <APIResponseErrorMessage errorStatus={this.state.errorStatus} error={this.state.error}/>
                }
            </div>
        ) ;
    }
}

export default LoginForm;
