import {HOST} from '../../commons/hosts';
import RestApiClient from "../../commons/api/rest-client";


const endpoint = {
    login: '/login',
    patient: '/patient/',
    caregiver: '/caregiver/',
    doctor: '/doctor/'
};

function login(email, password, callback) {
    let request = new Request(HOST.backend_api + endpoint.login+"?email="+email+"&password="+password, {
        method: 'GET',
    });
    console.log(request.url);
    RestApiClient.performRequest(request, callback);
}

function getPatientById(id,callback){
    let request = new Request(HOST.backend_api + endpoint.patient+id , {
            method: 'GET',
        });
     console.log(request.url);
     RestApiClient.performRequest(request, callback);
}

function getCaregiverById(id,callback){
    let request = new Request(HOST.backend_api + endpoint.caregiver+id , {
            method: 'GET',
        });
     console.log(request.url);
     RestApiClient.performRequest(request, callback);
}

function getDoctorById(id,callback){
    let request = new Request(HOST.backend_api + endpoint.doctor+id , {
            method: 'GET',
        });
     console.log(request.url);
     RestApiClient.performRequest(request, callback);
}





export {
    login,
    getCaregiverById,
    getDoctorById,
    getPatientById
};
