import React from 'react'
import {BrowserRouter as Router, Route, Switch} from 'react-router-dom'
import NavigationBar from './navigation-bar'
import Home from './home/home';
import PersonContainer from './person/person-container'
import PatientContainer from './patient/patient-container'
import CaregiverContainer from './caregiver/caregiver-container'
import MedicationContainer from './medication/medication-container'
import CaregiverPageContainer from './caregiverPage/caregiverPage-container'
import LoginContainer from './login/login-container'
import MedicationPlanContainer from './medicationPlan/medicationPlan-container'




import ErrorPage from './commons/errorhandling/error-page';
import styles from './commons/styles/project-style.css';

import {createBrowserHistory} from "history";

const history = createBrowserHistory({forceRefresh: true});

class DoctorPageContainer extends React.Component {


    render() {

        return (
            <div className={styles.back}>
            <Router history={history}>

                <div>
                    <NavigationBar />
                    <Switch>

                        <Route
                            exact
                            path='/doctorPage'
                            render={() => <Home/>}
                        />
                        <Route
                            exact
                            path='/patient'
                            render={() => <PatientContainer/>}
                        />

                        <Route
                            exact
                            path='/caregiver'
                            render={() => <CaregiverContainer/>}
                        />

                        <Route
                            exact
                            path='/medication'
                            render={() => <MedicationContainer/>}
                        />
                        <Route
                            exact
                            path='/medicationPlan'
                            render={() => <MedicationPlanContainer/>}
                        />


                        {/*Error*/}
                        <Route
                            exact
                            path='/error'
                            render={() => <ErrorPage/>}
                        />

                        <Route render={() =><ErrorPage/>} />
                    </Switch>
                </div>
            </Router>
            </div>
        )
    };
}

export default DoctorPageContainer
