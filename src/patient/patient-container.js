import React from 'react';
import APIResponseErrorMessage from "../commons/errorhandling/api-response-error-message";
import {
    Button,
    Card,
    CardHeader,
    Col,
    Modal,
    ModalBody,
    ModalHeader,
    Row
} from 'reactstrap';
import PatientForm from "./components/patient-form";
import DeletePatientForm from "./components/delete-patient-form";
import UpdatePatientForm from "./components/update-patient-form";


import * as API_USERS from "./api/patient-api"
import PatientTable from "./components/patient-table";



class PatientContainer extends React.Component {

    constructor(props) {
        super(props);
        this.toggleForm = this.toggleForm.bind(this);
        this.toggleForm2=this.toggleForm2.bind(this);
        this.toggleForm3=this.toggleForm3.bind(this);
        this.reload = this.reload.bind(this);
        this.reload2=this.reload2.bind(this);
        this.reload3=this.reload3.bind(this);
        this.state = {
            selected: false,
            delete_selected: false,
            update_selected:false,
            collapseForm: false,
            tableData: [],
            isLoaded: false,
            errorStatus: 0,
            error: null
        };
    }

    componentDidMount() {
        this.fetchPatients();
    }

    fetchPatients() {
        return API_USERS.getPatients((result, status, err) => {

            if (result !== null && status === 200) {
                this.setState({
                    tableData: result,
                    isLoaded: true
                });
            } else {
                this.setState(({
                    errorStatus: status,
                    error: err
                }));
            }
        });
    }

    toggleForm() {
        this.setState({selected: !this.state.selected});
    }

    toggleForm2(){
        this.setState({delete_selected: !this.state.delete_selected});
    }

     toggleForm3(){
            this.setState({update_selected: !this.state.update_selected});
        }


    reload() {
        this.setState({
            isLoaded: false
        });
        this.toggleForm();

        this.fetchPatients();
    }

       reload2() {
            this.setState({
                isLoaded:false
            });
            this.toggleForm2();
            this.fetchPatients();
        }

       reload3() {
            this.setState({
                isLoaded:false
            });
            this.toggleForm3();
            this.fetchPatients();
        }

    render() {
        return (
            <div>
                <CardHeader>
                    <strong> Patient Management </strong>
                </CardHeader>
                <Card>
                    <br/>
                    <Row>
                        <Col sm={{size: '8', offset: 1}}>
                            <Button color="primary" onClick={this.toggleForm}>Add Patient</Button>
                            <Button color="primary" onClick={this.toggleForm2}>Delete Patient</Button>
                            <Button color="primary" onClick={this.toggleForm3}>Update Patient</Button>
                        </Col>
                    </Row>
                    <br/>
                    <Row>
                        <Col sm={{size: '8', offset: 1}}>
                            {this.state.isLoaded && <PatientTable tableData = {this.state.tableData}/>}
                            {this.state.errorStatus > 0 && <APIResponseErrorMessage
                                                            errorStatus={this.state.errorStatus}
                                                            error={this.state.error}
                                                        />   }
                        </Col>
                    </Row>
                </Card>

                <Modal isOpen={this.state.selected} toggle={this.toggleForm}
                       className={this.props.className} size="lg">
                    <ModalHeader toggle={this.toggleForm}> Add Person: </ModalHeader>
                    <ModalBody>
                        <PatientForm reloadHandler={this.reload}/>
                    </ModalBody>
                </Modal>
                <Modal isOpen={this.state.delete_selected} toggle={this.toggleForm2}
                       className={this.props.className} size="lg">
                    <ModalHeader toggle={this.toggleForm2}> Delete Patient: </ModalHeader>
                    <ModalBody>
                        <DeletePatientForm reloadHandler={this.reload2}/>
                    </ModalBody>
                </Modal>

                <Modal isOpen={this.state.update_selected} toggle={this.toggleForm3}
                       className={this.props.className} size="lg">
                    <ModalHeader toggle={this.toggleForm3}> Update Patient: </ModalHeader>
                    <ModalBody>
                        <UpdatePatientForm reloadHandler={this.reload3}/>
                    </ModalBody>
                </Modal>


            </div>
        )

    }
}


export default PatientContainer;
