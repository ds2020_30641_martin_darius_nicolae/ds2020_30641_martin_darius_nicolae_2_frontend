import {HOST} from '../../commons/hosts';
import RestApiClient from "../../commons/api/rest-client";


const endpoint = {
    patientPage: '/patient/AllMedicationPlans/',
};

function getMedicationPlans(name,callback) {
    let request = new Request(HOST.backend_api + endpoint.patientPage+name, {
        method: 'GET',
    });
    console.log(request.url);
    RestApiClient.performRequest(request, callback);
}

export {
    getMedicationPlans,
};