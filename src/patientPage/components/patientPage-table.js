import React from "react";
import Table from "../../commons/tables/table";


const columns = [
    {
        Header: 'Name',
        accessor: 'name',
    },
    {
        Header: 'IntakeIntervals',
        accessor: 'intakeIntervals',
    },
    {
            Header: 'Period',
            accessor: 'period',
    },
    {
            Header: 'ListOfMedications',
            accessor: 'listOfMedications',
    },
    {
            Header: 'Pacientul',
            accessor: 'patient',
    }
];

const filters = [
    {
        accessor: 'name',
    }
];

class PatientPageTable extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            tableData: this.props.tableData
        };
    }

    render() {
        return (
            <Table
                data={this.state.tableData}
                columns={columns}
                search={filters}
                pageSize={5}
            />
        )
    }
}

export default PatientPageTable;