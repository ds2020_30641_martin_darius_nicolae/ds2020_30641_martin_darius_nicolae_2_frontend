import React from 'react';
import APIResponseErrorMessage from "../commons/errorhandling/api-response-error-message";
import {
    Button,
    Card,
    CardHeader,
    Col,
    Modal,
    ModalBody,
    ModalHeader,
    Row
} from 'reactstrap';

import * as API_USERS from "./api/patientPage-api"
import CaregiverPageTable from "./components/patientPage-table";

class PatientPageContainer extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            collapseForm: false,
            tableData: [],
            isLoaded: false,
            errorStatus: 0,
            error: null
        };
        this.mappingMed=this.mappingMed.bind(this);
    }

    componentDidMount() {
        this.fetchPatients();
    }

    fetchPatients() {
        return API_USERS.getMedicationPlans(this.props.match.params.name,(result, status, err) => {

            if (result !== null && status === 200) {
                let result2=result.map(this.mappingMed);
                this.setState({
                    tableData: result2,
                    isLoaded: true
                });
            } else {
                this.setState(({
                    errorStatus: status,
                    error: err
                }));
            }
        });
    }

     mappingMed = (object) => {
       let lista=[];
       object.listOfMedications.map((medicationObject,index) => {
        lista.push(medicationObject.name);
       });
       lista.join(',');
       var obj=   {
                name:object.name,
                intakeIntervals:object.intakeIntervals,
                period: object.period,
                listOfMedications:lista,
                patient:object.patientDTO.name,
                };

        return obj;

    }

    render() {
        return (
            <div>
                <CardHeader>
                    <strong> Patient Page Management </strong>
                </CardHeader>
                <Card>
                    <Row>
                        <Col sm={{size: '8', offset: 1}}>
                            {this.state.isLoaded && <CaregiverPageTable tableData = {this.state.tableData}/>}
                            {this.state.errorStatus > 0 && <APIResponseErrorMessage
                                                            errorStatus={this.state.errorStatus}
                                                            error={this.state.error}
                                                        />   }
                        </Col>
                    </Row>
                </Card>

            </div>
        )

    }
}


export default PatientPageContainer;