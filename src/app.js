import React from 'react'
import {BrowserRouter as Router, Route, Switch} from 'react-router-dom'
import NavigationBar from './navigation-bar'
import Home from './home/home';
import PersonContainer from './person/person-container'
import PatientContainer from './patient/patient-container'
import CaregiverContainer from './caregiver/caregiver-container'
import MedicationContainer from './medication/medication-container'
import CaregiverPageContainer from './caregiverPage/caregiverPage-container'
import LoginContainer from './login/login-container'
import MedicationPlanContainer from './medicationPlan/medicationPlan-container'

import DoctorPageContainer from './doctor'
import PatientPageContainer from './patientPage/patientPage-container'



import ErrorPage from './commons/errorhandling/error-page';
import styles from './commons/styles/project-style.css';

import {createBrowserHistory} from "history";

const history = createBrowserHistory({forceRefresh: true});

class App extends React.Component {


    render() {

        return (
            <div className={styles.back}>
            <Router history={history}>

                <div>

                    <Switch>

                        <Route
                            exact
                            path='/'
                            render={() => <LoginContainer/>}
                        />

                        <Route
                            exact
                            path='/patient'
                            render={() => <PatientContainer/>}
                        />

                        <Route
                            exact
                            path='/caregiver'
                            render={() => <CaregiverContainer/>}
                        />

                        <Route
                            exact
                            path='/medication'
                            render={() => <MedicationContainer/>}
                        />
                        <Route
                            exact
                            path='/medicationPlan'
                            render={() => <MedicationPlanContainer/>}
                        />

                        <Route path="/caregiverPage/:name" component={CaregiverPageContainer} />
                        <Route path="/doctorPage" component={DoctorPageContainer} />
                        <Route path="/patientPage/:name" component={PatientPageContainer} />


                        {/*Error*/}
                        <Route
                            exact
                            path='/error'
                            render={() => <ErrorPage/>}
                        />

                        <Route render={() =><ErrorPage/>} />
                    </Switch>
                </div>
            </Router>
            </div>
        )
    };
}

export default App
